//
//  ViewController.swift
//  Activity3_Weather
//
//  Created by Akhil S Raj on 2019-11-04.
//  Copyright © 2019 Akhil S Raj. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import WatchConnectivity
import Particle_SDK

class ViewController: UIViewController, WCSessionDelegate{
    
    
    var API_KEY = "43804b94deb07b8fae4f7b7ecc676a18"
    var cityName:String = ""
    var timeZone:String = ""
    var currentTime = ""
    
    let USERNAME = "akhils619@gmail.com"
    let PASSWORD = "Flyhigh@619"
    
    let DEVICE_ID = "440035001047363333343437"
    var myPhoton : ParticleDevice?
    
    var time = ""
    var minute = ""
    var temperature = ""
    var precipitation = ""
    var tommorowTemperature = ""
    
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var currentWeatherLabel: UILabel!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var tommorowTempLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if WCSession.isSupported() {
            print("\nPhone supports WCSession")
            WCSession.default.delegate = self
            WCSession.default.activate()
            print("\nSession activated")
        }
        else {
            print("Phone does not support WCSession")
            print("\nPhone does not support WCSession")
        }
        
        ParticleCloud.init()
        ParticleCloud.sharedInstance().login(withUser: self.USERNAME, password: self.PASSWORD) { (error:Error?) -> Void in
            if (error != nil) {
                print("Wrong credentials or as! ParticleCompletionBlock no internet connectivity, please try again")
                print(error?.localizedDescription as Any)
            }
            else {
                print("Login success!")
                self.loadDeviceFromCloud()
            }
        }
    }
    
    func loadDeviceFromCloud() {
        ParticleCloud.sharedInstance().getDevice(self.DEVICE_ID) { (device:ParticleDevice?, error:Error?) in
            
            if (error != nil) {
                print("Could not get device")
                print(error?.localizedDescription as Any)
                return
            }
            else {
                print("Got photon from cloud: \(String(describing: device?.id))")
                self.myPhoton = device
                self.subscribeToParticleEvents()
            }
        }
    }
    
    func subscribeToParticleEvents() {
        print("Inside subscribe method")
        var handler : Any?
        handler = ParticleCloud.sharedInstance().subscribeToDeviceEvents(
            withPrefix: "display",
            deviceID:self.DEVICE_ID,
            handler: {
                (event :ParticleEvent?, error : Error?) in
                
                if let _ = error {
                    print("could not subscribe to events")
                } else {
                    print("got event with data \(event?.data)")
                    let userControl = (event?.data)!
                    print("The usercontrol received from particle is \(userControl)")
                    if (userControl == "displayTime") {
                        self.displayTime()
                    }
                    else if (userControl == "displayTemperature"){
                        self.displayTemperarture()
                    }
                    else if (userControl == "displayPrecipitation") {
                        self.displayPrecipitation()
                        
                    }else if (userControl == "displayPrecipitation") {
                        self.displayTomorrowTemp()
                    }
                }
        })
    }
    
    
    
    
    func getWeatherData() {
        print("Invoked weather data API")
        let URL = "https://api.openweathermap.org/data/2.5/weather?q=\(self.cityName)&appid=\(API_KEY)"
        print(URL)
        Alamofire.request(URL).responseJSON {
            (currentWeather) in
            print(currentWeather.value!)
            
            let jsonResponse = JSON(currentWeather.value!)
            let cityName = jsonResponse["name"]
            let description = jsonResponse["weather"]
            let temp = jsonResponse["main"]["temp"]
            let desc = description[0]["main"]
            self.precipitation = desc.description
            self.cityLabel.text = cityName.description
            self.temperatureLabel.text = temp.description
           
            self.currentWeatherLabel.text = desc.description
             self.temperature = temp.description
            
        }
    }
    
    func getNextDayWeatherData() {
        print("Invoked weather data API")
        let URL = "https://api.openweathermap.org/data/2.5/forecast/daily?q=\(self.cityName)&mode=json&units=metric&cnt=2&APPID=\(self.API_KEY)"
        print(URL)
        Alamofire.request(URL).responseJSON {
            (currentWeather) in
            print(currentWeather.value!)
            
            let jsonResponse = JSON(currentWeather.value!)
            let cityName = jsonResponse["name"]
            let description = jsonResponse["weather"]
            let temp = jsonResponse["main"]["temp"]
            let desc = description[0]["main"]
            self.cityLabel.text = cityName.description
            self.tommorowTempLabel.text = temp.description
            self.tommorowTemperature = temp.description
            
        }
    }
    
    func getTime(){
        print("Invoked world time API")
        let URL = "https://worldtimeapi.org/api/\(self.timeZone)/"
        //print(URL)
        Alamofire.request(URL).responseJSON {
            (currentTimeReceived) in
            print(currentTimeReceived)
            let responseTime = JSON(currentTimeReceived.value)
            let dateTime = responseTime["datetime"]
            print("Date time received \(dateTime)")
            
            self.getHourTime(time: dateTime.string!)
            self.currentTimeLabel.text = dateTime.string!
        }
    }
    
    
    func displayTime() {
        print("Invoked display time method with time \(self.time)")
        var castTime:Int = Int(self.time)!
        var castMinute:Int = Int(self.minute)!
//        var timeInHours:String = ""
//        var parameters = [""]
//        if castTime < 12 {
//            let index = self.time.index(self.time.startIndex, offsetBy: 1)
//            let timeInHours  = self.time[index...]
//
//        }else {
//            parameters = [timeInHours]
        //}
         let parameters = [castTime]
        _ = myPhoton!.callFunction("hour", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
        
        print("Invoked display time method with time \(self.time)")
        let param = [castMinute]
        _ = myPhoton!.callFunction("min", withArguments: param) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
    }
    
    func displayTemperarture() {
        print("Invoked displayTemperarture method with time \(self.temperature)")
        let parameters = [self.temperature]
        _ = myPhoton!.callFunction("temp", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
    }
    
    func displayPrecipitation() {
        print("Invoked displayPrecipitation method with time \(self.precipitation)")
        
        let parameters = [self.precipitation]
        _ = myPhoton!.callFunction("prec", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
    }
    
    func displayTomorrowTemp() {
        print("Pressed the change lights button")
        let parameters = [self.tommorowTemperature]
        _ = myPhoton!.callFunction("temp", withArguments: parameters) {
            (resultCode : NSNumber?, error : Error?) -> Void in
            if (error == nil) {
                print("Sent message to Particle to turn green")
            }
            else {
                print("Error when telling Particle to turn green")
            }
        }
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {
            print("\nMessage Received: \(message)")
            self.cityName =  (message["cityname"] as? String)!
            self.timeZone =  (message["timezone"] as? String)!
            if self.cityName != ""{
                self.getWeatherData()
            }
            if self.timeZone != "" {
                self.getTime()
            }
            
        }
    }
    
    func getHourTime(time: String){
        var time = time.components(separatedBy: "T")
        var timeData = time[1].components(separatedBy: ".")
        var hourData = timeData[0].components(separatedBy: ":")
        self.time = hourData[0]
         self.minute = hourData[1]
        print("The hour data is \(self.time)")
    }
    
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    
}

