//
//  InterfaceController.swift
//  Activity3_Weather WatchKit Extension
//
//  Created by Akhil S Raj on 2019-11-04.
//  Copyright © 2019 Akhil S Raj. All rights reserved.
//

import WatchKit
import Foundation
import Alamofire
import WatchConnectivity

class InterfaceController: WKInterfaceController, WCSessionDelegate{
    
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    var city = ""
    var timeZone = ""
    
    @IBOutlet weak var selectCityButton: WKInterfaceButton!
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        if WCSession.isSupported() {
            WCSession.default.delegate = self
            WCSession.default.activate()
            print("Watch connection activated")
        }
        
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func selectCityPresses() {
        
        let cityList = ["Toronto", "Brisbane","Shanghai","Kolkata", "Rome", "Auckland"]
        presentTextInputController(withSuggestions: cityList, allowedInputMode: .plain) {
            
            (results) in
            
            if (results != nil && results!.count > 0) {
                // 2. write your code to process the person's response
                let userResponse = results?.first as? String
                self.city = userResponse!
                
                if self.city == "Toronto" {
                    self.timeZone = "America/Toronto"
                }else  if self.city == "Shanghai" {
                    self.timeZone = "Asia/Shanghai"
                } else if self.city == "Brisbane" {
                    self.timeZone = "Australia/Brisbane"
                }else if self.city == "Rome" {
                    self.timeZone = "Europe/Rome"
                }else if self.city == "Auckland" {
                    self.timeZone = "Pacific/Auckland"
                }else if self.city == "Kolkata" {
                    self.timeZone = "Asia/Kolkata"
                }
                
                self.sendCityName()
            }
        }
    }
    
    func sendCityName() {
        print("City name send to phone\(self.city)")
         print("Timezone send to phone\(self.timeZone)")
        if (WCSession.default.isReachable == true) {
            let message = ["cityname":self.city,
                           "timezone": self.timeZone] as [String : Any]
            WCSession.default.sendMessage(message, replyHandler:nil)
        }
    }
}
